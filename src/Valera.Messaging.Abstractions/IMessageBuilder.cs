﻿using System;
using Valera.Messaging.Abstractions.Endpoints;
using Valera.Messaging.Abstractions.Messages;

namespace Valera.Messaging.Abstractions
{
    internal interface IMessageBuilder
    {
        Message Build<TMessage>(string topic, TMessage message) where TMessage : class, IMessage;
        Message Build<TMessage>(string topic, TMessage message, IEndpoint replyToEndpoint) where TMessage : class, IMessage;

        Message Build<TMessage>(string topic, Guid correlationId, TMessage message)
            where TMessage : class, IMessage;
    }
}