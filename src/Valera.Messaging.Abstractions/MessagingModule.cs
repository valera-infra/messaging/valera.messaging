﻿using Autofac;
using Valera.Messaging.Abstractions.Endpoints;
using Valera.Messaging.Abstractions.Subscriptions;

namespace Valera.Messaging.Abstractions
{
    public class MessagingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EndpointRegistry>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SubscriptionsRegistry>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<CommandBus>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<EventBus>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<MessageBuilder>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
