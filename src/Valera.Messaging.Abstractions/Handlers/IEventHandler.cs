﻿using System.Threading.Tasks;
using Valera.Messaging.Abstractions.Messages;

namespace Valera.Messaging.Abstractions.Handlers
{
    public interface IEventHandler<TEvent> where TEvent : IEvent
    {
        Task Handle(TEvent @event);
    }
}
