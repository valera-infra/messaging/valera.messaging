﻿using System.Threading.Tasks;
using Valera.Messaging.Abstractions.Messages;

namespace Valera.Messaging.Abstractions.Handlers
{
    public interface ICommandHandler<TCommand>
        where TCommand : class, ICommand
    {
        Task Handle(TCommand command);
    }

    public interface ICommandHandler<TCommand, TResponse>
        where TCommand : class, ICommand
        where TResponse : class, IMessage
    {
        Task<TResponse> Handle(TCommand command);
    }
}