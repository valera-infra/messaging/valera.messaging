﻿using System;
using System.Threading.Tasks;
using Valera.Messaging.Abstractions.Messages;
using Valera.Messaging.Abstractions.Subscriptions;

namespace Valera.Messaging.Abstractions
{
    public interface IEventBus
    {
        Task<ISubscription> Subscribe<TEvent>(string service, Func<TEvent, Task> handler) where TEvent : class, IEvent;

        Task Publish<TEvent>(TEvent @event) where TEvent : class, IEvent;
    }
}